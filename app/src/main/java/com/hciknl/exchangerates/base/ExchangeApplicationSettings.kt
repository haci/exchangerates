package com.hciknl.exchangerates.base

import com.hciknl.core.CoreSettings

/**
 * Created by Hacı Kanal on 14.04.2019
 */
class ExchangeApplicationSettings : CoreSettings() {

    companion object {
        private const val API_URL = "http://data.fixer.io/api/"
        private const val HTTP_BODY_LOGGER_ENABLED_STATUS = true
    }

    override fun enableHttpBodyLogger() = HTTP_BODY_LOGGER_ENABLED_STATUS

    override fun endPoint() = API_URL
}