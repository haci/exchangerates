package com.hciknl.exchangerates.base

import android.app.Activity
import com.hciknl.core.CoreApplication
import com.hciknl.core.di.CoreComponent
import com.hciknl.exchangerates.base.di.DaggerExchangeAppComponent
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

/**
 * Created by Hacı Kanal on 14.04.2019
 */
class ExchangeRateApplication : CoreApplication(), HasActivityInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun createDependentAppModule(coreComponent: CoreComponent) {
        DaggerExchangeAppComponent
            .builder()
            .coreComponent(coreComponent)
            .build()
            .inject(this)
    }

    override fun coreSettings() = ExchangeApplicationSettings()

    override fun activityInjector() = activityInjector
}