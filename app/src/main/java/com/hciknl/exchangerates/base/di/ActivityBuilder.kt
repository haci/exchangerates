package com.hciknl.exchangerates.base.di

import com.hciknl.exchangerates.feature.dashboard.DashboardModule
import com.hciknl.exchangerates.feature.dashboard.presentation.DashboardActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Hacı Kanal on 14.04.2019
 */
@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [DashboardModule::class])
    abstract fun bindMainActivity(): DashboardActivity
}