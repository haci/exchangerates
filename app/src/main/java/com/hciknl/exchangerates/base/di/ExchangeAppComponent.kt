package com.hciknl.exchangerates.base.di

import com.hciknl.core.di.CoreComponent
import com.hciknl.core.di.scopes.AppScope
import com.hciknl.exchangerates.base.ExchangeRateApplication
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule

/**
 * Created by Hacı Kanal on 14.04.2019
 */
@AppScope
@Component(
    dependencies = [CoreComponent::class],
    modules = [ActivityBuilder::class, AndroidSupportInjectionModule::class]
)
interface ExchangeAppComponent {

    fun inject(app: ExchangeRateApplication)
}