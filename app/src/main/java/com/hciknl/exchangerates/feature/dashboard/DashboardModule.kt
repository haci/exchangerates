package com.hciknl.exchangerates.feature.dashboard

import com.hciknl.exchangerates.feature.dashboard.data.DashboardApiService
import com.hciknl.exchangerates.feature.dashboard.data.DashboardDataRepository
import com.hciknl.exchangerates.feature.dashboard.data.source.DashboardRemoteDataSource
import com.hciknl.exchangerates.feature.dashboard.domain.DashboardRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * Created by Hacı Kanal on 14.04.2019
 */
@Module
class DashboardModule {

    @Provides
    fun provideRemoteDataSource(retrofit: Retrofit): DashboardRemoteDataSource =
        DashboardRemoteDataSource(retrofit.create(DashboardApiService::class.java))

    @Provides
    fun provideDataRepository(remoteDataSource: DashboardRemoteDataSource): DashboardRepository =
        DashboardDataRepository(remoteDataSource)
}