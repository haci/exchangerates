package com.hciknl.exchangerates.feature.dashboard.data

import com.hciknl.exchangerates.feature.dashboard.data.model.CurrencyResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Hacı Kanal on 14.04.2019
 */
interface DashboardApiService {

    //    @GET("timeSeries?access_key=6c3ba2519901da204cbebbf8c818ccc9") // Current plan does not support this query
    @GET("{date}?access_key=6c3ba2519901da204cbebbf8c818ccc9")
    fun getLatestCurrencyAsync(
        @Path("date") date: String,
        @Query("symbols") symbols: String
    ): Deferred<CurrencyResponse>
}