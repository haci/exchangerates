package com.hciknl.exchangerates.feature.dashboard.data

import com.hciknl.exchangerates.feature.dashboard.data.model.CurrencyResponse
import com.hciknl.exchangerates.feature.dashboard.data.source.DashboardRemoteDataSource
import com.hciknl.exchangerates.feature.dashboard.domain.DashboardRepository
import com.hciknl.exchangerates.feature.dashboard.domain.interactors.CurrencyQuery
import javax.inject.Inject

/**
 * Created by Hacı Kanal on 14.04.2019
 */
class DashboardDataRepository @Inject constructor(
    private val remoteDataSource: DashboardRemoteDataSource
) : DashboardRepository {

    override suspend fun getHistoricalRateChangeList(params: CurrencyQuery.Params): CurrencyResponse {
//        GlobalScope.launch {
//            val howMany = params.howMany
//
// //            val list = mutableListOf<CurrencyResponse>()
// //            for (i in 1..howMany) {
// //                list.add(remoteDataSource.createAsync(params))
// //            }
//        }
//        return remoteDataSource.fetchAsync(params)
//        successHandler(remoteDataSource.createAsync(params).await(), remoteDataSource.createAsync(params).await())
        return CurrencyResponse()
    }

    fun successHandler(resp: CurrencyResponse, resp2: CurrencyResponse) {
        println("$resp second $resp2")
    }

    override suspend fun getLatestCurrency(params: CurrencyQuery.Params): CurrencyResponse {
        return remoteDataSource.fetch(params)
    }
}