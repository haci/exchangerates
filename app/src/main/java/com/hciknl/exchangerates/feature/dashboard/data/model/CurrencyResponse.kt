package com.hciknl.exchangerates.feature.dashboard.data.model

import com.hciknl.core.network.CoreResponse
import com.hciknl.exchangerates.feature.dashboard.presentation.CurrencyEntityMap
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Created by Hacı Kanal on 18.04.2019
 * Base currency selection is restricted by API plan.
 */
@JsonClass(generateAdapter = true)
data class CurrencyResponse(
    @Json(name = "success") val isSuccess: Boolean = false,
    @Json(name = "historical") val isHistorical: Boolean = false,
    @Json(name = "timestamp") val timeStamp: Long = 0,
    @Json(name = "base") val baseCurrency: String = "",
    @Json(name = "date") val date: String = "",
    @Json(name = "rates") val rates: Map<String, Double> = hashMapOf()
) : CoreResponse<CurrencyEntityMap>() {

    override fun toEntity(): CurrencyEntityMap {
        val map = CurrencyEntityMap()
        if (rates.isNotEmpty()) {
            map.values["EUR"] = rates["EUR"] ?: 0.00
            map.values["USD"] = rates["USD"] ?: 0.00
        }
        return map
    }
}