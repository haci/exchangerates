package com.hciknl.exchangerates.feature.dashboard.data.source

import com.hciknl.core.CoreRemoteSource
import com.hciknl.exchangerates.feature.dashboard.data.DashboardApiService
import com.hciknl.exchangerates.feature.dashboard.data.model.CurrencyResponse
import com.hciknl.exchangerates.feature.dashboard.domain.interactors.CurrencyQuery
import kotlinx.coroutines.Deferred
import java.util.Arrays
import javax.inject.Inject

/**
 * Created by Hacı Kanal on 14.04.2019
 */
class DashboardRemoteDataSource @Inject constructor(private val api: DashboardApiService) :
    CoreRemoteSource<CurrencyQuery.Params, CurrencyResponse>() {

    override fun createAsync(params: CurrencyQuery.Params): Deferred<CurrencyResponse> {
        return api.getLatestCurrencyAsync(params.date, convertParams(params.symbols))
    }

    private fun convertParams(symbols: Array<String>): String =
        Arrays.toString(symbols).replace("[", "").replace("]", "")
}