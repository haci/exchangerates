package com.hciknl.exchangerates.feature.dashboard.domain

import com.hciknl.exchangerates.feature.dashboard.data.model.CurrencyResponse
import com.hciknl.exchangerates.feature.dashboard.domain.interactors.CurrencyQuery

/**
 * Created by Hacı Kanal on 14.04.2019
 */
interface DashboardRepository {

    suspend fun getHistoricalRateChangeList(params: CurrencyQuery.Params): CurrencyResponse

    suspend fun getLatestCurrency(params: CurrencyQuery.Params): CurrencyResponse
}