package com.hciknl.exchangerates.feature.dashboard.domain.interactors

import com.hciknl.exchangerates.feature.dashboard.data.model.CurrencyResponse
import com.hciknl.exchangerates.feature.dashboard.domain.DashboardRepository
import javax.inject.Inject

/**
 * Created by Hacı Kanal on 14.04.2019
 */
class CurrencyQuery @Inject constructor(
    private val repository: DashboardRepository
) {

    suspend fun fetchHistoricalRate(params: Params): CurrencyResponse {
        return repository.getHistoricalRateChangeList(params)
    }

    suspend fun fetchLatestRate(params: Params): CurrencyResponse {
        return repository.getLatestCurrency(params)
    }

    data class Params(
        val howMany: Int = 0,
        val date: String = "",
        val symbols: Array<String> = arrayOf("USD", "EUR")
    )
}