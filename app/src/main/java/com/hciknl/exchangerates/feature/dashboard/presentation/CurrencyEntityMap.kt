package com.hciknl.exchangerates.feature.dashboard.presentation

/**
 * Created by Hacı Kanal on 2019-04-20
 */

class CurrencyEntityMap {
    val values = mutableMapOf<String, Double>()
}