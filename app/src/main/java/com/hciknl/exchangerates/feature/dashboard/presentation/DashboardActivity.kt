package com.hciknl.exchangerates.feature.dashboard.presentation

import android.os.Bundle
import com.hciknl.core.CoreActivity
import com.hciknl.exchangerates.R
import kotlinx.android.synthetic.main.activity_dashboard.*
import javax.inject.Inject

class DashboardActivity : CoreActivity<DashboardViewModel>() {

    @Inject
    lateinit var factory: DashboardViewModelFactory

    var i = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
//        viewModel.getHistoricalExchange(2)
//        viewModel.getLatestRate()

        clickme.setOnClickListener {
            textView.text = "$i"
            i++
        }
    }

    override fun getViewModelBuilder() = Builder(factory, DashboardViewModel::class.java)
}
