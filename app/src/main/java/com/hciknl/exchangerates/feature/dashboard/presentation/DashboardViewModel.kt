package com.hciknl.exchangerates.feature.dashboard.presentation

import com.hciknl.core.CoreViewModel
import com.hciknl.core.DateHelper
import com.hciknl.exchangerates.feature.dashboard.domain.interactors.CurrencyQuery
import kotlinx.coroutines.launch
import java.util.*

class DashboardViewModel(
    private val currencyQuery: CurrencyQuery,
    private val dateHelper: DateHelper
) : CoreViewModel() {

    fun getHistoricalExchange(numberOfDaysForPast: Int) {
        customScope.launch {
            val dates = dateHelper.findDatesBetween(Date(), numberOfDaysForPast)
            dates.forEach {
                currencyQuery.fetchHistoricalRate(CurrencyQuery.Params(date = it)).toEntity()
            }
        }
        println("getHistoricalExchange")
    }

    /**
     * Returns today EUR - USD rate
     */
    fun getLatestRate() {
        val now = dateHelper.formatDate(Date())
        customScope.launch {
            val response = currencyQuery.fetchLatestRate(CurrencyQuery.Params(date = now)).toEntity()
//            println("test $response")
        }
    }
}
