package com.hciknl.exchangerates.feature.dashboard.presentation

import com.hciknl.core.CoreViewModelFactory
import com.hciknl.core.DateHelper
import com.hciknl.exchangerates.feature.dashboard.domain.interactors.CurrencyQuery
import javax.inject.Inject

/**
 * Created by Hacı Kanal on 17.04.2019
 */
class DashboardViewModelFactory @Inject constructor(
    private val currencyQuery: CurrencyQuery,
    private val dateHelper: DateHelper
) :
    CoreViewModelFactory<DashboardViewModel>() {

    override fun provideViewModel() = DashboardViewModel(currencyQuery, dateHelper)
}