package com.hciknl.exchangerates.feature.dashboard

import com.hciknl.exchangerates.feature.dashboard.data.DashboardApiService
import com.hciknl.exchangerates.feature.dashboard.data.source.DashboardRemoteDataSource
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Answers
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Retrofit

/**
 * Created by Hacı Kanal on 17.04.2019
 */
@RunWith(MockitoJUnitRunner::class)
class DashboardModuleTest {

    @Mock
    lateinit var retrofit: Retrofit

    @Mock
    lateinit var service: DashboardApiService

    @Mock(answer = Answers.CALLS_REAL_METHODS)
    lateinit var module: DashboardModule

    @Test
    fun testRemoteSourceCreation() {
        // Given

        // When
        `when`(retrofit.create(DashboardApiService::class.java)).thenReturn(service)
        val dataSource = module.provideRemoteDataSource(retrofit)

        // Then
        verify(retrofit).create(DashboardApiService::class.java)
        assertNotNull(dataSource)
    }

    @Test
    fun testDataRepositoryCreation() {
        // Given
        val mockRemote = mock(DashboardRemoteDataSource::class.java)
        // When
        val repo = module.provideDataRepository(mockRemote)

        // Then
        assertNotNull(repo)
    }
}