package com.hciknl.exchangerates.feature.dashboard.data

import com.hciknl.exchangerates.feature.dashboard.data.model.CurrencyResponse
import com.hciknl.exchangerates.feature.dashboard.data.source.DashboardRemoteDataSource
import com.hciknl.exchangerates.feature.dashboard.domain.interactors.CurrencyQuery
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by Hacı Kanal on 18.04.2019
 */
@RunWith(MockitoJUnitRunner::class)
class DashboardDataRepositoryTest {

    @Mock
    lateinit var remote: DashboardRemoteDataSource

    @Mock
    lateinit var mockResponse: CurrencyResponse

    @InjectMocks
    lateinit var repository: DashboardDataRepository

    @Test
    fun testLatestCurrencyCallFromRepository() {
        runBlocking {
            // Given
            val params = CurrencyQuery.Params(date = "2011-12-20")

            // When
            Mockito.`when`(remote.fetch(params)).thenReturn(mockResponse)
            repository.getLatestCurrency(params)

            // Then
            Mockito.verify(remote).fetch(params)
            Unit
        }
    }
}