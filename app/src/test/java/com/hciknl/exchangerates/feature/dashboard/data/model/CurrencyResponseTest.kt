package com.hciknl.exchangerates.feature.dashboard.data.model

import org.junit.Assert
import org.junit.Test

/**
 * Created by Hacı Kanal on 2019-04-20
 */
class CurrencyResponseTest {

    @Test
    fun testEntityMapper() {
        // Given
        val response = CurrencyResponse()

        // When
        val mappedObject = response.toEntity()

        // Then
        Assert.assertNotNull(mappedObject)
    }
}