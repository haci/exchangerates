package com.hciknl.exchangerates.feature.dashboard.data.source

import com.hciknl.exchangerates.feature.dashboard.data.DashboardApiService
import com.hciknl.exchangerates.feature.dashboard.data.model.CurrencyResponse
import com.hciknl.exchangerates.feature.dashboard.domain.interactors.CurrencyQuery
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by Hacı Kanal on 18.04.2019
 */
@RunWith(MockitoJUnitRunner::class)
class DashboardRemoteDataSourceTest {

    @Mock
    lateinit var api: DashboardApiService

    @Mock
    lateinit var deferred: Deferred<CurrencyResponse>

    @InjectMocks
    lateinit var remote: DashboardRemoteDataSource

    @Test
    fun testLatestCurrencyCallFromRepository() = runBlocking {
        // Given
        val params = CurrencyQuery.Params(date = "2011")
        val symbolsCaptor = argumentCaptor<String>()
        val dateCaptor = argumentCaptor<String>()

        // When
        Mockito.`when`(api.getLatestCurrencyAsync(any(), any())).thenReturn(deferred)
        remote.fetch(params)

        // Then
        Mockito.verify(api).getLatestCurrencyAsync(dateCaptor.capture(), symbolsCaptor.capture())

        Assert.assertEquals(params.date, dateCaptor.firstValue)
        Assert.assertEquals("USD, EUR", symbolsCaptor.firstValue)
    }
}