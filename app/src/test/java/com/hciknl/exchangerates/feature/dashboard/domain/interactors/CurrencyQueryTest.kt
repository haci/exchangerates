package com.hciknl.exchangerates.feature.dashboard.domain.interactors

import com.hciknl.exchangerates.feature.dashboard.domain.DashboardRepository
import com.nhaarman.mockitokotlin2.argumentCaptor
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by Hacı Kanal on 18.04.2019
 */
@RunWith(MockitoJUnitRunner::class)
class CurrencyQueryTest {

    @Mock
    lateinit var repository: DashboardRepository

    @InjectMocks
    lateinit var currencyQuery: CurrencyQuery

    @Test
    fun testLatestCurrencyCallFromRepository() = runBlocking {
        // Given
        val givenDate = "2011"
        val params = CurrencyQuery.Params(date = givenDate)
        val paramsCaptor = argumentCaptor<CurrencyQuery.Params>()
        // When
        currencyQuery.fetchLatestRate(params)

        // Then
        verify(repository).getLatestCurrency(paramsCaptor.capture())

        Assert.assertEquals(params.date, paramsCaptor.firstValue.date)
        Assert.assertArrayEquals(params.symbols, paramsCaptor.firstValue.symbols)
    }
}