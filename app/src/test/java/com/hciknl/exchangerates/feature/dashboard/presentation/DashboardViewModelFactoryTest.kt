package com.hciknl.exchangerates.feature.dashboard.presentation

import com.hciknl.core.DateHelper
import com.hciknl.exchangerates.feature.dashboard.domain.interactors.CurrencyQuery
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by Hacı Kanal on 17.04.2019
 */
@RunWith(MockitoJUnitRunner::class)
class DashboardViewModelFactoryTest {

    @Mock
    lateinit var currencyQuery: CurrencyQuery

    @Mock
    lateinit var dateHelper: DateHelper

    @InjectMocks
    lateinit var viewModelFactory: DashboardViewModelFactory

    @Test
    fun testViewModelCreation() {
        // Given

        // When
        val vm = viewModelFactory.provideViewModel()

        // Then
        assertNotNull(vm)
    }
}