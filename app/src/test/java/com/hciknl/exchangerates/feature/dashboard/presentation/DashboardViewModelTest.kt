package com.hciknl.exchangerates.feature.dashboard.presentation

import com.hciknl.core.DateHelper
import com.hciknl.exchangerates.feature.dashboard.domain.interactors.CurrencyQuery
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.times
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Answers
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by Hacı Kanal on 2019-04-21
 */
@RunWith(MockitoJUnitRunner::class)
class DashboardViewModelTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    lateinit var currencyQuery: CurrencyQuery

    @Mock
    lateinit var dateHelper: DateHelper

    lateinit var viewModel: DashboardViewModel

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        viewModel = DashboardViewModel(currencyQuery, dateHelper)
    }

    @Test
    fun testHistoricalCurrencyQuery() {
        runBlocking {
            // Given
            val howManyDays = 10
            val paramsCaptor = argumentCaptor<CurrencyQuery.Params>()
            val list = mutableListOf<String>()
            for (i in 1..howManyDays) {
                list.add("$i")
            }

            // When
            `when`(dateHelper.findDatesBetween(any(), any())).thenReturn(list)
            viewModel.getHistoricalExchange(howManyDays)

            // Then
            verify(currencyQuery, times(howManyDays)).fetchHistoricalRate(paramsCaptor.capture())

            assertEquals(howManyDays, paramsCaptor.firstValue.howMany)
        }
    }

    @Test
    fun testLatestCurrencyQuery() {
        runBlocking {
            // Given
            val dummyDate = "20202020"

            // When
            `when`(dateHelper.formatDate(any())).thenReturn(dummyDate)
            viewModel.getLatestRate()

            // Then
            verify(currencyQuery).fetchLatestRate(any())
            verify(dateHelper).formatDate(any())
        }

    }
}