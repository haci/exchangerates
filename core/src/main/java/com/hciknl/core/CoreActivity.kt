package com.hciknl.core

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.AndroidInjection

/**
 * Created by Hacı Kanal on 14.04.2019
 */
abstract class CoreActivity<VM : ViewModel> : AppCompatActivity() {

    lateinit var viewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        val (factory, clazz) = getViewModelBuilder()
        viewModel = ViewModelProviders.of(this, factory).get(clazz)
    }

    abstract fun getViewModelBuilder(): Builder<VM>

    data class Builder<VM>(val factory: ViewModelProvider.Factory, val clazz: Class<VM>)
}