package com.hciknl.core

import android.app.Application
import com.hciknl.core.di.CoreComponent
import com.hciknl.core.di.DaggerCoreComponent
import java.util.Objects

/**
 * Created by Hacı Kanal on 14.04.2019
 */
abstract class CoreApplication : Application() {

    private lateinit var coreComponent: CoreComponent

    override fun onCreate() {
        super.onCreate()

        val settings = coreSettings()
        Objects.requireNonNull(settings, " Settings cannot be null")

        coreComponent = DaggerCoreComponent
            .builder()
            .application(this)
            .settings(settings)
            .build()
        createDependentAppModule(coreComponent)
    }

    abstract fun coreSettings(): CoreSettings

    abstract fun createDependentAppModule(coreComponent: CoreComponent)
}