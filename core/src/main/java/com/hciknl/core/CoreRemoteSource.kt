package com.hciknl.core

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.coroutineScope

/**
 * Created by Hacı Kanal on 18.04.2019
 */
abstract class CoreRemoteSource<in Params, out Response> {

    /**
     * Waits a deferred object from network interface
     * Named async only for compiler warnings
     */
    abstract fun createAsync(params: Params): Deferred<Response>

    /**
     * starts http request
     */
    suspend fun fetch(params: Params): Response = coroutineScope {
        return@coroutineScope createAsync(params).await()
    }
}
