package com.hciknl.core

/**
 * Created by Hacı Kanal on 14.04.2019
 */
abstract class CoreSettings {

    /**
     * HttpClient logger disable/ enable state for logcat
     */
    abstract fun enableHttpBodyLogger(): Boolean

    /**
     * Api Base URL
     * Cannot be null
     */
    abstract fun endPoint(): String
}