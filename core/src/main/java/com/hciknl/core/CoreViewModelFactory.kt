package com.hciknl.core

import androidx.lifecycle.ViewModelProvider

/**
 * Created by Hacı Kanal on 17.04.2019
 */
abstract class CoreViewModelFactory<ViewModel : androidx.lifecycle.ViewModel> : ViewModelProvider.Factory {

    /**
     * Basic ViewModel creation
     */
    override fun <Type : androidx.lifecycle.ViewModel> create(modelClass: Class<Type>): Type {
        val viewModel = provideViewModel()

        if (modelClass.isAssignableFrom(viewModel.javaClass)) {
            return viewModel as Type
        }
        throw IllegalArgumentException("Unknown ViewModel class: " + modelClass.name)
    }

    /**
     * Instance of a viewModel
     */
    abstract fun provideViewModel(): ViewModel
}
