package com.hciknl.core

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import java.util.Date

/**
 * Created by Hacı Kanal on 2019-04-21
 */
class DateHelper {

    companion object {
        private const val DATE_FORMAT = "yyyy-MM-dd"
    }

    private var formatter: SimpleDateFormat

    init {
        formatter = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())
    }

    /**¬
     * Finds today's date and goes back to 'n' days
     * returns a list that contains 'n' days as related string format
     */
    fun findDatesBetween(startDate: Date, howMany: Int): List<String> {
        val calendar = Calendar.getInstance()

        calendar.time = startDate
        calendar.add(Calendar.DATE, -howMany)

        val dateList = mutableListOf<String>()

        while (calendar.time.before(startDate)) {
            val date = formatDate(calendar.time)
            dateList.add(date)
            calendar.add(Calendar.DATE, 1)
        }

        return dateList
    }

    /**
     * Formats given date with [DATE_FORMAT]
     */
    fun formatDate(date: Date) = formatter.format(date)
}