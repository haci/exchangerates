package com.hciknl.core.di

import com.hciknl.core.CoreApplication
import com.hciknl.core.CoreSettings
import com.hciknl.core.DateHelper
import com.hciknl.core.di.scopes.CoreScope
import dagger.BindsInstance
import dagger.Component
import retrofit2.Retrofit

/**
 * Created by Hacı Kanal on 14.04.2019
 */
@CoreScope
@Component(
    modules = [CoreModule::class, CoreNetworkModule::class]
)
interface CoreComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: CoreApplication): Builder

        @BindsInstance
        fun settings(settings: CoreSettings): Builder

        fun build(): CoreComponent
    }

    fun retrofit(): Retrofit

    fun dateHelper(): DateHelper
}