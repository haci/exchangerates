package com.hciknl.core.di

import com.hciknl.core.DateHelper
import com.hciknl.core.di.scopes.CoreScope
import dagger.Module
import dagger.Provides

/**
 * Created by Hacı Kanal on 14.04.2019
 */
@Module
class CoreModule {

    @Provides
    @CoreScope
    fun provideDateHelper(): DateHelper = DateHelper()
}