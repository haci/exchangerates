package com.hciknl.core.di

import com.hciknl.core.CoreSettings
import com.hciknl.core.di.scopes.CoreScope
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
class CoreNetworkModule {

    @Provides
    @CoreScope
    fun provideHttpClient(coreSettings: CoreSettings): OkHttpClient {
        val client = OkHttpClient.Builder()
        addLoggingInterceptor(coreSettings, client)
        return client.build()
    }

    @Provides
    @CoreScope
    fun provideRetrofit(coreSettings: CoreSettings, client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .baseUrl(coreSettings.endPoint())
            .build()
    }

    private fun addLoggingInterceptor(coreSettings: CoreSettings, clientBuilder: OkHttpClient.Builder) {
        if (coreSettings.enableHttpBodyLogger()) {
            val logger = HttpLoggingInterceptor()
            logger.level = HttpLoggingInterceptor.Level.BODY
            clientBuilder.addInterceptor(logger)
        }
    }
}
