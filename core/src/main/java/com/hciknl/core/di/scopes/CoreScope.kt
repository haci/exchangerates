package com.hciknl.core.di.scopes

import javax.inject.Scope

/**
 * Created by Hacı Kanal on 14.04.2019
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class CoreScope