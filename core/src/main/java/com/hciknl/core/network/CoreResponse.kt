package com.hciknl.core.network

/**
 * Created by Hacı Kanal on 2019-04-20
 */
abstract class CoreResponse<out Entity> {

    /**
     * Converts Data Layer object to UI Layer one
     * Also mapping
     */
    abstract fun toEntity(): Entity
}