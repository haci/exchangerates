package com.hciknl.core

import org.junit.Assert
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

/**
 * Created by Hacı Kanal on 2019-04-21
 */
class DateHelperTest {

    @Test
    fun testFormatter() {
        // Given
        val dateHelper = DateHelper()
        val format = "yyyy-MM-dd"
        val time = System.currentTimeMillis()
        val date = Date(time)
        // When
        val now = dateHelper.formatDate(date)

        // Then
        Assert.assertEquals(SimpleDateFormat(format, Locale.getDefault()).format(date), now)
    }

    @Test
    fun testFindDatesBetweenRange() {
        // Given
        val dateHelper = DateHelper()
        val howManyDaysWillBeFind = 10

        // When
        val priorDays = dateHelper.findDatesBetween(Date(), howManyDaysWillBeFind)

        // Then
        Assert.assertEquals(howManyDaysWillBeFind, priorDays.size)
    }
}